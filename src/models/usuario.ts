export interface Usuario {
    sobrenome: string;
    email: string;
}