export class Plano {
    id: number;
    nome: string;
    minutos: number;
    porcentagem: number = 10;
    valor: number;
}