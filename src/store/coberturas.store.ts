import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules';
import store from './index';
import { EntityState, EntityAdapter } from 'vue-entity-adapter-plus';
import { Cobertura } from '@/models/cobertura';

interface State extends EntityState<Cobertura> {
	loadingList: boolean;
	totalItens: number;
}

const stateAdapter = new EntityAdapter<Cobertura>();

@Module
class CoberturaVuex extends VuexModule {
	// state
	state: State = {
		...stateAdapter.getInitialState(),
		loadingList: false,
		totalItens: 0
	};

	// getters
	get coberturas() {
		return this.state.entities;
	}

	get carregando() {
		return this.state.loadingList;
	}

	// mutations
	@Mutation
	private listado(payload: Cobertura[]) {
		const { entities, ids } = stateAdapter.addMany(payload, this.state);
		this.state.entities = entities;
		this.state.ids = ids;
	}

	// actions
	@Action
	async listar() {
		this.state.loadingList = true;
		const coberturas: Cobertura[] = [
			{ id: 1, origem: '011', destino: '016', valor: 1.9 },
			{ id: 2, origem: '016', destino: '011', valor: 2.9 },
			{ id: 3, origem: '011', destino: '017', valor: 1.7 },
			{ id: 4, origem: '017', destino: '011', valor: 2.7 },
			{ id: 5, origem: '011', destino: '018', valor: 0.9 },
			{ id: 6, origem: '018', destino: '011', valor: 1.9 }
		];
		this.listado(coberturas);
		this.state.loadingList = false;
	}
}

export const CoberturaStore = new CoberturaVuex({ store, name: 'coberturas' });
