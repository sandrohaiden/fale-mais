import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import store from "./index";
import { EntityState, EntityAdapter } from 'vue-entity-adapter-plus';
import { Cobertura } from '@/models/cobertura';
import { Plano } from '@/models/plano';

interface State extends EntityState<Plano> {
  loadingList: boolean;
  totalItens: number;
}

const stateAdapter = new EntityAdapter<Plano>();

@Module
class PlanoVuex extends VuexModule {

  // state
  state: State = {
    ...stateAdapter.getInitialState(),
    loadingList: false,
    totalItens: 0
  };

  // getters
  get planos() {
    return this.state.entities;
  }

  get carregando() {
    return this.state.loadingList;
  }

  // mutations
  @Mutation
  private listado(payload: Plano[]) {
    const { entities, ids } = stateAdapter.addMany(payload, this.state);
    this.state.entities = entities;
    this.state.ids = ids;
  }

  // actions
  @Action
  async listar() {
    this.state.loadingList = true;
    const planos: Plano [] = [
      {id: 1, nome: 'FaleMais30', minutos: 30, porcentagem: 10, valor: 30},
      {id: 2, nome: 'FaleMais60', minutos: 60, porcentagem: 10, valor: 60},
      {id: 3, nome: 'FaleMais120', minutos: 120, porcentagem: 10, valor: 120}
    ]
    //if(this.state.entities.length < 1) {
      this.listado(planos);
    //}
    this.state.loadingList = false;
  }

}

export const PlanoStore = new PlanoVuex({ store, name: "planos" });
