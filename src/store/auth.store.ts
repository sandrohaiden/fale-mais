import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { Usuario } from "@/models/usuario";
import { UsuarioLogin } from "@/models/usuarioLogin";
import store from "./index";
import { openSnack } from '@/_helpers/utils';

export interface UsuarioState {
  usuario: Usuario | null;
  loging: boolean | false;
  token: string | null;
}

// const usuarioAdapter = new EntityAdapter<Usuario>();

@Module
class AuthVuex extends VuexModule {

  // state
  state: UsuarioState = {
    usuario: null,
    token: null,
    loging: false,
  };

  // getters
  get usuario() {
    return this.state.usuario;
  }

  get token() {
    return this.state.token;
  }

  // mutations
  @Mutation
  private logged(payload: UsuarioState) {
    this.state.usuario = payload.usuario;
    this.state.token = payload.token;
    localStorage.setItem('accessToken', payload.token as string);
    openSnack({msg: 'Login realizado com sucesso', color: 'success'})
  }

  @Mutation
  private changeToken(token: string) {
    this.state.token = token;
  }

  // actions
  @Action
  async initLogin(dadosLogin: UsuarioLogin) {
    if(dadosLogin.email === 'admin@admin' && dadosLogin.senha === '123') {
      this.logged(dadosLogin as any);
        this.state.loging = false;
    }
  }

  @Action
  async setToken(token: string){
    this.changeToken(token);
  }

  @Action
  async userChecked(payload: UsuarioState) {
    this.logged(payload);
  }
}

// register module (could be in any file)

export const AuthModule = new AuthVuex({ store, name: "auth" });
