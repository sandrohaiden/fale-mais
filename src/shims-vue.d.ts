declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

/* Vuetify não está apto por padrão para trabalhar em conjunto com o TypeScript
  é necessário importar este módulo para que o problema seja solucionado.
  Solução encontrada em: https://medium.com/@JonUK/creating-a-mobile-web-app-with-vue-vuetify-typescript-dc69bed4cd2d
*/
declare module 'vuetify/lib' {
  import 'vuetify/types/lib'
}