import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Vantagens from "../views/vantagens/Vantagens.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Vantagens
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login/Login.vue"),
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
