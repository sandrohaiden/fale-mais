import EventBus from './eventBus';

export function openSnack(data: {msg: string; color?: string}){
  EventBus.$emit('snack', data)
}
